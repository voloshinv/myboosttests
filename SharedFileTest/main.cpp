#include <iostream>

#include <fstream>
#include <cstdio>

//#include <stdio.h>

#include <thread>
#include <chrono>
#include <cstdlib>
#include <string>
#include <boost/interprocess/sync/file_lock.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
//#include <boost/filesystem.hpp>

//using namespace std;
using namespace boost::interprocess;

#define FILENAME_TO_TEST    "sync.txt"

int main(int argc, char* argv[])
{
    if (argc > 2){
        std::cout << "bad usage";
    }

    int process_id = 0;

    //автоматический запуск сопроцессов
    if (argc == 2 )
    {
        process_id = atoi(argv[1]);
        if (process_id > 0)
        {
            std::string cmd;

            #ifndef __unix
            cmd.append("start ");
            #else
            cmd.append(argv[0]);
            #endif

            cmd.append(" ");
            cmd.append(std::to_string(process_id - 1));

            #ifdef __unix
            cmd.append(" &");
            #endif

            std::cout << cmd << std::endl;

            std::system(cmd.c_str());
        }
    }


    std::fstream stream;
    stream.open(FILENAME_TO_TEST, std::ios_base::in | std::ios_base::out | std::ios_base::app);

    if (stream.fail()){
        std::cerr << "Failed to open/create " FILENAME_TO_TEST << std::endl;
        stream.close();
        return 1;
    }

    //отключаем потоковую буфферизацию, увеличивает вероятность коллизий
    stream.setf(std::ios_base::unitbuf);

    stream.exceptions ( std::ifstream::failbit | std::ifstream::badbit );

    try
    {
        file_lock flock(FILENAME_TO_TEST);

        for (int i = 0; i < 100; i++)
        {
            {
                scoped_lock<file_lock> e_lock(flock);

                const char* symbol = "testing process ";

                //Выводим в поток посимвольно, это увеличивает вероятность возникновения коллизий
                //но уменьшает общую скорость записи
                while(*symbol) stream.put(*(symbol++));

                //stream << symbol;
                stream << std::to_string(process_id) << std::endl;

                stream.flush();

                if (stream.fail()){
                    std::cerr << "Failed to write" << std::endl;
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

    }
    catch( std::exception &ex)
    {
        std::cerr << "exception: " << ex.what() << std::endl;
    }

    stream.close();

    //подождем, пока остальные процессы завершатся
    if (process_id == 0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        #ifndef __unix
        //std::system("notepad " FILENAME_TO_TEST);
        #else
        std::system("leafpad " FILENAME_TO_TEST);
        #endif

        std::remove(FILENAME_TO_TEST);

    }

    //std::cout << "press Enter to quit" << std::endl;
    //std::cin.get();

    return 0;
}

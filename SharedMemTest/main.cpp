#include <iostream>
#include <boost/interprocess/shared_memory_object.hpp>

//using namespace std;
using namespace boost::interprocess;

#define MY_MEMORY_NAME "MySharedMemory"

int main()
{
    struct shm_remove
    {
        shm_remove() { shared_memory_object::remove(MY_MEMORY_NAME); }
        ~shm_remove(){ shared_memory_object::remove(MY_MEMORY_NAME); }
    } remover;

    shared_memory_object shm_obj(create_only, MY_MEMORY_NAME, read_write);

    //set size
    shm_obj.truncate(10000);

    std::cout << "Hello World!" << std::endl;
    return 0;
}

